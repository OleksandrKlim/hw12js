"use strict";
let input = document.body;
input.addEventListener("keypress", (event) => {
  let inputKey = event.key;
  let key = document.getElementsByClassName("btn");
  for (const li of key) {
    if (li.innerText === inputKey || li.innerText.toLowerCase() === inputKey) {
      li.style.backgroundColor = "blue";
    } else {
      if (li.style.backgroundColor === "blue") {
        li.style.backgroundColor = "red";
      } else {
        li.style.backgroundColor = "";
      }
    }
  }
});
